/*
Cinq hommes de nationalités et de professions différentes habitent des maisons de couleurs différentes et situés côte à côte dans le même alignement. Ils ont chacun un animal favori et une boisson préférée.

Les données du problème sont :
- l'anglais habite la maison rouge
- l'espagnol a un chien
- dans la maison verte on boit du café
- l'ukrainien boit du thé
- la maison verte est immédiatement à droite de la maison blanche
- le sculpteur élève des escargots
- le diplomate habite la maison jaune
- dans la maison du milieu on boit du lait
- le norvégien habite la première maison à gauche
- le médecin habite une maison voisine de celle où demeure le propriétaire du renard
- la maison du diplomate est à côté de celle où il y a un cheval
- le violoniste boit du jus d'orange
- le japonais est acrobate
- le norvégien habite à côté de la maison bleue

Les problématiques à résoudre sont :
- Qui boit de l'eau ?
- Qui possède le zèbre ?
*/

:- module(problem2, [resoudre/0]).
:- use_module(helpers).

% La rue est une liste de 5 maisons où l'ordre compte :
rue([
  maison(_, _, _, _, _),
  maison(_, _, _, _, _),
  maison(_, _, _, _, _),
  maison(_, _, _, _, _),
  maison(_, _, _, _, _)
]).

resoudre :-
  % étant donnée la rue décrite ci-dessus :
  rue(Rue),
  % l'anglais habite la maison rouge :
  member(maison(rouge, anglais, _, _, _), Rue),
  % l'espagnol a un chien :
  member(maison(_, espagnol, _, chien, _), Rue),
  % dans la maison verte on boit du café :
  member(maison(verte, _, _, _, 'café'), Rue),
  % l'ukrainien boit du thé :
  member(maison(_, ukrainien, _, _, 'thé'), Rue),
  % la maison verte est immédiatement à droite de la maison blanche :
  on_the_right_of(maison(verte, _, _, _, _), maison(blanche, _, _, _, _), Rue),
  % le sculpteur élève des escargots :
  member(maison(_, _, sculpteur, escargots, _), Rue),
  % le diplomate habite la maison jaune :
  member(maison(jaune, _, diplomate, _, _), Rue),
  % dans la maison du milieu on boit du lait :
  Rue = [_, _, maison(_, _, _, _, lait), _, _],
  % le norvégien habite la première maison à gauche :
  Rue = [maison(_, 'norvégien', _, _, _), _, _, _, _],
  % le médecin habite une maison voisine de celle où demeure le propriétaire du renard :
  next_to(maison(_, _, 'médecin', _, _), maison(_, _, _, renard, _), Rue),
  % la maison du diplomate est à côté de celle où il y a un cheval :
  next_to(maison(_, _, diplomate, _, _), maison(_, _, _, cheval, _), Rue),
  % le violoniste boit du jus d'orange :
  member(maison(_, _, violoniste, _, "jus d'orange"), Rue),
  % le japonais est acrobate :
  member(maison(_, japonais, acrobate, _, _), Rue),
  % le norvégien habite à côté de la maison bleue :
  next_to(maison(_, 'norvégien', _, _, _), maison(bleue, _, _, _, _), Rue),
  % le cinquième animal est un zèbre (d'après la problématique) :
  member(maison(_, _, _, 'zèbre', _), Rue),
  % la cinquième boisson est de l'eau (d'après la problématique) :
  member(maison(_, _, _, _, eau), Rue),
  % affiche la ou les rue(s) possible(s) :
  writeln("Rue :"), write_list(Rue).
