/*
Les données du problème sont :
- l'anglais vit dans la maison rouge.
- le jaguar est l'animal de l'espagnol.
- le japonais vit à droite de la maison du possesseur de l'escargot.
- le possesseur de l'escargot vit à gauche de la maison bleue.

La problématique à résoudre est :
- Qui possède le serpent ?
*/

:- module(problem1, [resoudre/0]).
:- use_module(helpers).

% La rue est une liste de 3 maisons, et l'ordre ne compte pas :
rue([
  maison(rouge, _, _),
  maison(bleu, _, _),
  maison(vert, _, _)
]).

resoudre :-
  % étant donnée la rue décrite ci-dessus :
  rue(Rue),
  % l'anglais est dans la maison rouge :
  member(maison(rouge, anglais, _), Rue),
  % l'espagnol possède le jaguar :
  member(maison(_, espagnol, jaguar), Rue),
  % le japonais ne possède pas l'escargot :
  subset([maison(_, _, escargot), maison(_, japonais, _)], Rue),
  % l'escargot n'est pas dans la maison bleue :
  subset([maison(bleu, _, _), maison(_, _, escargot)], Rue),
  % le troisième animal est un serpent :
  member(maison(_, _, serpent), Rue),
  % affiche la ou les rue(s) possible(s) :
  writeln("Rue :"), write_list(Rue).
