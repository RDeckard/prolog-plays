:- module(helpers, [write_list/1, nonmember/2, on_the_left_of/3, on_the_right_of/3, next_to/3, before/3, after/3]).

write_list([]) :- !.
write_list([Head|Tail]) :- writeln(Head), write_list(Tail).

nonmember(_, []) :- !.
nonmember(A, [Head | Tail]) :- A \== Head, nonmember(A, Tail).

on_the_left_of(A, B, [A, B | _]).
on_the_left_of(A, B, [_ | Tail]) :- on_the_left_of(A, B, Tail).

on_the_right_of(A, B, [B, A | _]).
on_the_right_of(A, B, [_ | Tail]) :- on_the_right_of(A, B, Tail).

next_to(A, B, List) :- on_the_left_of(A, B, List); on_the_right_of(A, B, List).

before(A, B, [A | Tail]) :- member(B, Tail).
before(A, B, [_ | Tail]) :- before(A, B, Tail).

after(A, B, [B | Tail]) :- member(A, Tail).
after(A, B, [_ | Tail]) :- after(A, B, Tail).
