/*
Cinq sportifs sont dans la salle d'attente d'un médecin spécialisé.

Retrouvez grâce aux indications suivantes leur ordre d'arrivée, le sport pratiqué par chacun ainsi que la raison de leur présence :

- Jean qui est arrivé en dernier, ne pratique ni la gymnastique ni le basket. Il ne vient pas se faire établir un certificat
- Christian, venu pour un soin, ne pratique pas non plus la gymnastique
- Grégoire, qui est arrivé avant Laurent mais après celui qui pratique la gymnastique fait du patinage. Il n'est venu ni pour un certificat, ni chercher un renouvellement d'ordonnance
- Celui qui pratique le football est arrivé le troisième, avant Christian. Il est venu chercher une dispense
- Remi est un sportif. Le rugby est un sport. La visite est un motif de consultation.
*/

:- module(problem3, [resoudre/0]).
:- use_module(helpers).

% Il y a 5 sportifs dans la salle, où l'ordre d'arrivée compte.
salle([
  sportif(_, _, _),
  sportif(_, _, _),
  sportif(_, _, _),
  sportif(_, _, _),
  sportif(_, _, _)
]).

resoudre :-
  salle(Sportifs),
  % On associe les sports et motifs utilisés uniquement négativement plus bas :
  member(sportif(_, basket, _), Sportifs),
  member(sportif(_, _, certificat), Sportifs),
  member(sportif(_, _, renouvellement), Sportifs),
  % Jean est arrivé en dernier :
  Sportifs = [_, _, _, _, sportif(jean, SportDeJean, MotifDeJean)],
  % Jean ne pratique ni la gymnastique ni le basket
  nonmember(SportDeJean, [gymnastique, basket]),
  % Jean ne vient pas se faire établir un certificat :
  MotifDeJean \== certificat,
  % Christian est venu pour un soin :
  member(sportif(christian, SportDeChristian, soin), Sportifs),
  % Christian ne pratique pas la gymnastique :
  SportDeChristian \== gymnastique,
  % Grégoire est arrivé avant Laurent :
  before(sportif('grégoire', _, _), sportif(laurent, _, _), Sportifs),
  % Grégoire est arrivé après celui qui pratique la gymnastique :
  after(sportif('grégoire', _, _), sportif(_, gymnastique, _), Sportifs),
  % Grégoire fait du patinage :
  member(sportif('grégoire', patinage, MotifDeGregoire), Sportifs),
  % Grégoire n'est venu ni pour un certificat, ni chercher un renouvellement d'ordonnance :
  nonmember(MotifDeGregoire, [certificat, renouvellement]),
  % Celui qui pratique le football est arrivé troisième et est venu chercher une dispense :
  Sportifs = [_, _, sportif(_, football, dispense), _, _],
  % Celui qui pratique le football est arrivé avant Christian :
  before(sportif(_, football, _), sportif(christian, _, _), Sportifs),
  % Remi est un sportif :
  member(sportif('rémi', _, _), Sportifs),
  % Le rugby est un sport :
  member(sportif(_, rugby, _), Sportifs),
  % La visite est un motif de consultation :
  member(sportif(_, _, visite), Sportifs),
  % affiche la ou les salle(s) possible(s) :
  writeln("Salle :"), write_list(Sportifs).
