data(intel).
data(amd) :- !.
data(arm).

compare_cut_1(X) :- data(X).
compare_cut_1('apple M1').


compare_cut_2(X) :- data(X), !.
compare_cut_2('apple M1').

not_2(X) :- X = intel.
not_2(X) :- not(X = intel).

compare_cut_3(X,Y) :- data(X), !, data(Y).
compare_cut_3('apple M1').

not_3(X,Y) :- X = intel, data(Y).
not_3(X,Y) :- not(X = intel), data(Y).
